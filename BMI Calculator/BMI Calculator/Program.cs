﻿using System;

namespace BMI_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Welcoming messages. Getting weight and height from the user and converts values to double. 
            Console.WriteLine("Hi! Welcome to BMI Calculator!");
            Console.WriteLine("Please enter your weight in kg: ");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Please enter your height in cm: ");
            double height = Convert.ToDouble(Console.ReadLine());

            // Calculating BMI-score and multiplying weight and height with 100 to get correct values.
            double score = ((weight * 100) / (height * height) * 100);

            // Printing out the BMI-score and rounding the score that it has two decimals.
            Console.WriteLine("Your BMI-score is: {0}", Math.Round(score, 2));

            // Checks if score is less than 18.5 - printing out underweight-message.
            if (score < 18.5)
            {
                Console.WriteLine("You are underweight.");
            }
            // Checks is score is between 18.5 and or equal to 24.9 - printing out normal weight-message.
            else if (score > 18.5 && score <= 24.9)
            {
                Console.WriteLine("You are normal weight.");
            }
            // Checks if score is between 25 and or equal 29.9 - printing out overweight-message.
            else if (score > 25 && score <= 29.9)
            {
                Console.WriteLine("You are overweight.");
            }
            // If score is larger than 29.9 - printing out obese-message.
            else
            {
                Console.WriteLine("You are obese.");
            }
        }
    }
}
