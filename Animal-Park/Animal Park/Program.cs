﻿using System;
using System.Collections.Generic;

namespace Animal_Park
{
    class Program
    {
        static void Main(string[] args)
        {
           
            // Creating 3 different animals from the Animal class, and giving them attributes. 
            Animal dog = new Animal("Lukas", "woooff");
            Animal cat = new Animal("Tim", "meeeeoow");
            Animal horse = new Animal("Biggie", "neigh");

            // Creating a new list with Animal.
            List<Animal> AnimalList = new List<Animal>();

            // Adding 3 animals to AnimalList.
            AnimalList.Add(dog);
            AnimalList.Add(cat);
            AnimalList.Add(horse);

            // Using foreach printing out the owners of the animals in the list. 
            foreach (var Animal in AnimalList)
            {
                Console.WriteLine(Animal.Owner);
            }
           
    
        }
    }
}
