﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal_Park
{
    class Animal
    {
        private string owner;
        private string sound { get; set; }
        public string Owner { get => owner; set => owner = value; }

        public Animal(string owner)
        {
            this.owner = owner;
        }

        public Animal(string owner, string sound)
        {
            this.sound = sound;
            this.owner = owner;
        }

        public string getOwner()
        {
            return owner;
        }
    }
}
