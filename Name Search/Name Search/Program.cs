﻿using System;
using System.Collections.Generic;

namespace Name_Search
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creating an empty list named classList with capacity of 5.
            List<string> classList = new List<string>(5);


            //Adding 5 users into classList.
            classList.Add("Isaac Newton");
            classList.Add("Stephen Hawking");
            classList.Add("Albert Einstein");
            classList.Add("Nikola Tesla");
            classList.Add("Elon Musk");

            //Getting the search-input from the user
            Console.WriteLine("Enter searchword: ");
            string searchWord = (Console.ReadLine());



            // Iterates the list checking if the items contains searchword.
            foreach (var item in classList)
            {
                // If items contains a searhword, printing out the item.
                if (item.Contains(searchWord))
                    Console.WriteLine(item);

            }


            // Printing out the names in the list using a foreach loop. 
            foreach (string people in classList)
                Console.WriteLine(people);
        }
    }
}
