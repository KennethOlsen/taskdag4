﻿using System;

namespace Even_Fibonacci_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int first = 1;
            int previous = 2;
            int current = first + previous;
            int sum = 2;
            while (current < 4000000)
            {
                first = previous;
                previous = current;
                current = previous + first;
                if (current % 2 == 0)
                    sum = sum + current;
            }

            Console.WriteLine("The sum is{0]: " + sum);
        }
    }
}


