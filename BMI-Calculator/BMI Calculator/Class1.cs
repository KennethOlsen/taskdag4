﻿using System;
using System.Collections.Generic;
//using System.Text;

namespace BMI_Calculator
{
    class BMI
    {
        //Fields of class.
        private double height;
        private double weight;

        // Creating setters for height and weight.
        public double Height {set => height = value; }
        public double Weight { set => weight = value; }

        // Constructor. Setting the object of BMI to correct state when initialized. 
        public BMI(double height, double weight)
        {
            this.height = height;
            this.weight = weight;
        }

        // Method for getting the BMI-score using the mathmatic formula. 
        public double getScore()
        {
            double score = ((weight*100) / (height * height)*100);
            return score;
        }
    }
}

