﻿using System;

namespace BMI_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Welcoming messages. Getting weight and height from the user and converts values to double. 
            Console.WriteLine("Enter weight in kg: ");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter height in cm: ");
            double height = Convert.ToDouble(Console.ReadLine());

            // Printing out the BMI-score and rounding the score that it has two decimals.
            BMI obj = new BMI(height, weight);
            
           // Console.WriteLine("Your BMI-score is: {0}", Math.Round(obj.getScore(), 2));
            Console.WriteLine("Your BMI-score is: {0}", Math.Round(obj.getScore(), 2));

            // Checks if score is less than 18.5 - printing out underweight-message.
            if (obj.getScore() < 18.5)
            {
                Console.WriteLine("You are underweight.");
            }
            // Checks is score is between 18.5 and or equal to 24.9 - printing out normal weight-message.
            else if (obj.getScore() > 18.5 && obj.getScore() <= 24.9)
            {
                Console.WriteLine("You are normal weight.");
            }
            // Checks if score is between 25 and or equal 29.9 - printing out overweight-message.
            else if (obj.getScore() > 25 && obj.getScore() <= 29.9)
            {
                Console.WriteLine("You are overweight.");
            }
            // If score is larger than 29.9 - printing out obese-message.
            else
            {
                Console.WriteLine("You are obese.");
            }
        }

       
        
    }
}
