﻿using System;

namespace Square
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hi! Welcome to SquareMaker!");
            Console.WriteLine("Please enter length of your square: ");
            int intLength = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter width of your square: ");
            int intWidth = Convert.ToInt32(Console.ReadLine());


            for (int i = 0; i < intWidth; i++)
            {

                if (i == 0 || i == intWidth - 1)
                {
                    string outline = new string('#', intLength);
                    Console.WriteLine(outline);
                }
                else
                {
                    string middle = new string(' ', intWidth - 2);
                    Console.WriteLine('#' + middle + '#');
                }
            }
        }
    }
}
